from django.db import models

class Mileage(models.Model):
    start_mileage = models.PositiveIntegerField(null=False, default=0)
    start_time = models.DateTimeField(null=True)
    stop_mileage = models.PositiveIntegerField(null=False, default=0)
    stop_time = models.DateTimeField(null=True)
    reporter = models.CharField(null=False, max_length=20)
    completed = models.BooleanField(default=False)
