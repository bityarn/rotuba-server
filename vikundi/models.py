from django.db import models
import logging
from django.db.models.signals import pre_save, post_save
from rapidsms_xforms.models import xform_received

class Kikundi(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField("group_name", max_length=30)

class Mkulima(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField("member_name", max_length=30)
    age = models.CharField("age", max_length=65)
    mobile_number = models.CharField("mobile_number", max_length=65)
    group = models.CharField("group_name", max_length=65)
    location = models.CharField("gps_coordinates", max_length=65)
    farm_name = models.CharField("farm_name", max_length=65)
    farm_size = models.CharField("farm_size", max_length=65)
    farm_partitions = models.CharField("farm_partitions", max_length=65)


# define a listener
def bityarn_xform_received_handler(sender, **kwargs):
    xform = kwargs['xform']
    submission = kwargs['submission']


    logging.error('%s is the submission object' % submission)
    

    if xform.keyword == 'produce': #&& not submission.has_errors:
	logging.error('Produce Registration')

        return

    if xform.keyword == 'farmreg':
        logging.error('Farmer Registration')
	
	farmer_name = submission.values.get(attribute__name='Farmer Name').value
	farmer_age = submission.values.get(attribute__name='Age').value
	farm_size = submission.values.get(attribute__name='Farm size').value
	#farm_partitions = submission.values.get(attribute__name='Number of partitions').value
	#farm_location = submission.values.get(attribute__name='Farm Location').value
	
	Mkulima(name=farmer_name, age=farmer_age,farm_size=farm_size).save()

	#logging.error(farm_partitions)
	#import pdb;
	#pdb.set_trace()
	
        return

    if xform.keyword == 'grpreg':
        logging.error('Group Registration')
	group_name = submission.values.get(attribute__name='Group Name').value
	Kikundi(name=group_name).save()

        return

# then wire it to the xform_received signal
xform_received.connect(bityarn_xform_received_handler, weak=True)
