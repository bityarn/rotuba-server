from django.db import models

class Mazao(models.Model):
    crop = models.CharField(null=False, max_length=20)
    quantity = models.PositiveIntegerField(null=False, default=0)
    received = models.DateTimeField(null=True)
    reporter = models.CharField(null=False, max_length=20)
