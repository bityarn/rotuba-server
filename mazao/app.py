# vim: ai sts=4 sw=4 ts=4 et
from rapidsms.apps.base import AppBase
import re
from models import *
from datetime import datetime

class App(AppBase):

    #pattern = re.compile(r'^mazao\s+(start|stop)\s+(\d+)', re.IGNORECASE)

    pattern = re.compile(r'^mazao\s+(A-za-z)\s+(\d+)', re.IGNORECASE)

    def start (self):
        """Configure your app in the start phase."""
        pass

    def parse (self, message):
        """Parse and annotate messages in the parse phase."""
        pass

    def handle (self, message):
        import pdb;
        
        response = self.pattern.findall(message.text)
        pdb.set_trace()
        if response:
            entry = response[0]
            crop = entry[1]
            quantity = int(entry[2])
            received = datetime.now()
            reporter = message.connection.identity

            # Persist entry in the database
            Mazao(crop=crop,quantity=quantity,received=received,reporter=reporter).save()
            # Generate a response
            message.respond("Asante kwa ripoti")

            return True
        else:
            return False


    def cleanup (self, message):
        """Perform any clean up after all handlers have run in the
           cleanup phase."""
        pass

    def outgoing (self, message):
        """Handle outgoing message notifications."""
        pass

    def stop (self):
        """Perform global app cleanup when the application is stopped."""
        pass
