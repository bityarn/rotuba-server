To get started, start a virtualenv, activate it and install rapidsms-xforms in the env as below:

% pip install rapidsms-xforms

Within the virtualenv, navigate to the app directory and run

% python manage.py syncdb

% python manage.py runserver

Then in a separate window, but within the virtualenv, run

% python manage.py runrouter

Navigate to 127.0.0.1:8000 to test your new install.
