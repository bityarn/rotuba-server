# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.http import require_GET
from vikundi.models import *

@require_GET
def index(req):
    groups = Kikundi.objects.all()
    members = Mkulima.objects.all()
    return render_to_response(
        "index.html",{'groups': groups,'members':members},
        context_instance=RequestContext(req))


